//index.js
//获取应用实例

const app = getApp()

Page({
  data: {
    userInfo: {},
    hasUserInfo: false
  },


  onLoad() {

  },

  //事件处理函数
  goToTest() {
    wx.navigateTo({
      url: '../test/test'
    })
  },

  goToDetails() {
    wx.navigateTo({
      url: '../details/details'
    })
  },

  goToHistory() {
    wx.navigateTo({
      url: '../history/history'
    })
  },

  goToRank() {
    wx.navigateTo({
      url: '../rank/rank'
    })
  },

  //微信授权登录
  login() {
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: (res) => {
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
        app.globalData.userInfo = res.userInfo
        app.globalData.hasUserInfo = true
      }
    })
  },

  onShareAppMessage(res) {
    return {
      title: '@你，快来参与消防安全知识答题活动吧~'
    }
  },
})
