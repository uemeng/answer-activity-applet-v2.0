# 答题活动小程序v2.0

#### 简介
答题活动小程序v2.0，基于云开发的微信答题小程序，软件架构是微信原生小程序+云开发。主要包含六大功能模块页面，首页、答题页、结果页、活动规则页、答题记录页、排行榜页。适用于交通安全答题、 消防安全知识宣传、 安全生产知识学习、答题活动、有奖答题等。

#### 技术栈
微信原生小程序+云开发

#### 当前版本

微信答题活动小程序，当前版本是v2.0

- 活动规则页
- 答题记录页
- 排行榜页
- 题库随机抽题
- 查询历史成绩
- 微信授权登录
- 获取微信头像和昵称等

- 首页、答题页、结果页
- 实现页面间跳转功能
- 实现转发分享答题成绩功能
- 实现用云开发实现查询题库功能
- 实现动态题目数据绑定
- 答题交互逻辑
- 切换下一题
- 提交答卷保存到云数据库集合
- 系统自动判分
- 答题结果页从云数据库查询答题成绩

<img src="qrcode.jpg" alt="答题小程序码" title="答题小程序码" width="26%" />


#### 界面截图

![输入图片说明](3-%E6%B0%B4%E5%8D%B0.jpg)

![输入图片说明](4-%E6%B0%B4%E5%8D%B0.jpg)

首页
![输入图片说明](20211125222854.png)

微信授权登录
![输入图片说明](20211125222923.png)

转发分享答题
![输入图片说明](20211125223754.png)

答题页
![输入图片说明](20211125223003.png)

结果页 
![输入图片说明](20211125223031.png)

转发分享成绩
![输入图片说明](20211125223825.png)

活动规则页
![输入图片说明](20211125223509.png)

答题记录页
![输入图片说明](20211125223100.png)

排行榜页
![输入图片说明](20211125223118.png)



#### 安装教程

1.  下载开发者工具
2.  导入小程序项目
3.  创建云开发环境


#### 手把手教你搭建教程

##### v1.0

[消防安全知识竞答活动小程序](https://developers.weixin.qq.com/community/develop/article/doc/0002ca223b8370d5c9fce586e56813)

[安全知识线上答题活动小程序-答题功能解读](https://developers.weixin.qq.com/community/develop/article/doc/0008ea6e43894864020d8452456c13)

[不破不立，分享源码，优质的消防安全知识竞答活动小程序](https://developers.weixin.qq.com/community/develop/article/doc/000c08d38205f04c3f0d25f075c013)

[手把手教你搭建消防安全答题小程序-首页](https://developers.weixin.qq.com/community/develop/article/doc/000242a28904a022570d57cc856c13)

[手把手教你搭建消防安全答题小程序-答题页](https://developers.weixin.qq.com/community/develop/article/doc/000c282e6d0a78b86c0d02af555413)

[手把手教你搭建消防安全答题小程序-答题结果页](https://developers.weixin.qq.com/community/develop/article/doc/00064c224f0560f9790d4069256c13)

[手把手教你搭建消防安全答题小程序-实现页面间跳转功能](https://developers.weixin.qq.com/community/develop/article/doc/0008400ebac4b0938d0d4ca8e5bc13)

[手把手教你搭建消防安全答题小程序-实现转发分享答题成绩功能](https://developers.weixin.qq.com/community/develop/article/doc/00064a97294000c29e0d1154d5b813)

[手把手教你搭建消防安全答题小程序-用云开发实现查询题库功能](https://developers.weixin.qq.com/community/develop/article/doc/0002cac6f7018077a00d7a36456413)

[手把手教你搭建消防安全答题小程序-将用云开发获取到的题目渲染到答题页面](https://developers.weixin.qq.com/community/develop/article/doc/000c04a413c49850d90d281ec56c13)

[手把手教你搭建消防安全答题小程序-实现答题功能以及提交答卷到云数据库](https://developers.weixin.qq.com/community/develop/article/doc/00006654a7cce0d2e40d174755b413)

[手把手教你搭建消防安全答题小程序-在结果页中实现从云数据库查询成绩并展示](https://developers.weixin.qq.com/community/develop/article/doc/000a0e480f86b012f60da776c51413)

[基于云开发的答题活动小程序v1.0，开开开源啦](https://developers.weixin.qq.com/community/develop/article/doc/0006c6920245e07df20dccbde56c13)

[基于云开发的微信答题活动小程序v1.0搭建部署帮助文档](https://developers.weixin.qq.com/community/develop/article/doc/000ce8d4ef0dc864791d0f2ce56c13)

[用云开发搭建的微信答题小程序v1.0](https://developers.weixin.qq.com/community/develop/article/doc/000a2adbde4918698e1d5b7405b013)

##### v2.0

[基于云开发的答题活动小程序v2.0，终于赶在11月最后一天完成了](https://developers.weixin.qq.com/community/develop/article/doc/000e6221f30fa852052dab1275b813)

[基于云开发的答题活动小程序v2.0-首页设计与实现](https://developers.weixin.qq.com/community/develop/article/doc/000440596f4268f7182d043ab5b813)

[基于云开发的答题活动小程序v2.0-实现微信授权登录功能](https://developers.weixin.qq.com/community/develop/article/doc/00082895780f6010282d655f756013)

[基于云开发的答题活动小程序v2.0-用云开发的聚合能力实现从题库中随机出题功能](https://developers.weixin.qq.com/community/develop/article/doc/000e0831a804f0eb202d4cf2451813)

[基于云开发的答题活动小程序v2.0-排行榜页面用云开发能力实现查询答题成绩并进行排名的功能](https://developers.weixin.qq.com/community/develop/article/doc/0000004ab784c0717b2d9163856013)

[基于云开发的答题活动小程序v2.0-答题记录页](https://developers.weixin.qq.com/community/develop/article/doc/0002467cbb066073af2da79345bc13)

[基于云开发的答题活动小程序v2.0-结合具体场景谈谈路由传参以及组件化思想](https://developers.weixin.qq.com/community/develop/article/doc/00060a7650c45863573d542b256013)

[基于云开发的答题活动小程序v2.0-完整项目分享（附源码）](https://developers.weixin.qq.com/community/develop/article/doc/00066428beca60c5403d6101e51c13)

[基于云开发的答题活动小程序v2.0-运行部署帮助文档](https://developers.weixin.qq.com/community/develop/article/doc/0000e6b5854d3019563d30cd959c13)


#### 使用说明

1. 对于这个UI好看的答题小程序，可以用于学习或者直接使用，也可以进行二开。
2. 该项目持续迭代中， **请点一下右上角的star** ，并且 **扫码赞赏一杯咖啡** ，进行加油鼓劲或者催更吧~

<img src="https://images.gitee.com/uploads/images/2021/1102/230811_faf72c80_1280216.jpeg" alt="赞赏码" title="赞赏码" width="26%" />

#### 开源版本v1、v2

版本v1.0、v2.0已完成

#### 付费版本v3

版本v3.0已完成

- 注册登录页 √
- 题库学习 √
- 支持单选、判断题型 √
- 错题集 √
- 查看所有用户的答题记录-管理员 √
- 查看用户的答题成绩以及答题情况-管理员 √
- 后台数据监控-管理员 √
- 后台管理-管理员 √

![输入图片说明](20220322093236-3.png)

注册登录页
![输入图片说明](20220322093346-3.png)

题库学习
![输入图片说明](20220322093523-3.png)

![输入图片说明](20220322093906-3.png)

![输入图片说明](20220322094002-3.png)

错题集
![输入图片说明](20220322094209-3.png)

![输入图片说明](20220322094449-3.png)

查看所有用户的答题记录-管理员
![输入图片说明](20220322094506-3.png)

查看其他用户的答题成绩以及答题情况-管理员
![输入图片说明](20220322094547-3.png)

![输入图片说明](20220322094558-3.png)

题库管理（新增、查看、搜索、编辑、删除、导入、导出）-管理员

题库列表
![输入图片说明](20220507230441.png)

条件筛选
![输入图片说明](20220507230704.png)

关键词搜索
![输入图片说明](20220507230759.png)

新增题目
![输入图片说明](20220507230849.png)

编辑题目
![输入图片说明](20220507231021.png)


#### 计划版本v4、v5

下一个迭代计划版本v4

- 选项乱序
- 限时作答
- 答题倒计时

下下一个迭代计划版本v5

- 支持生成海报分享
- 支持答题后抽奖

#### 联系

> 联系微信，meng674782630，请备注“码云”。如果是技术问题，请付费咨询，谢谢！

#### 心得体会
> 1. 找到你所能认识的最优秀的人，和他们一起做点事
> 2. 世界上最聪明的人是借用别人撞的头破血流的经验作为自己的经验


